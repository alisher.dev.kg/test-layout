import { test, expect, devices } from '@playwright/test';

const URL = 'http://localhost:8018';

const { viewport: mobileViewport } = devices['Pixel 5'];
test.use({ viewport: mobileViewport });

test.beforeEach(async ({ page }) => {
  await page.goto(URL);
});

test.describe('Mobile menu', () => {
  test('Opening', async ({ page }) => {
    const menuButton = page.locator('.menu-button-container');
    const navigation = page.locator('.navigation');

    await expect(menuButton).toBeVisible();

    await expect(navigation).not.toBeVisible();

    await menuButton.click();

    await expect(navigation).toBeVisible();
  });
});
