import fs from 'fs';
import path from 'path';
import captureWebsite from 'capture-website';

const viewports = [
  { width: 1440, height: 900 },
  { width: 768, height: 1024 },
  { width: 1024, height: 768 },
  { width: 320, height: 480 },
];
const URL = 'http://localhost:8018';
const screenshotsDir = './screenshots';

function start () {
  clearDir(screenshotsDir)

  viewports.forEach(viewport => {
    doScreenshot(URL, viewport)
  })
}
start()

function clearDir (directory) {
  fs.readdir(directory, (err, files) => {
    if (err) throw err;

    for (const file of files) {
      const isIndexFile = file === 'index.js'
      if (isIndexFile) {
        continue
      }

      fs.unlink(path.join(directory, file), err => {
        if (err) throw err;
      });
    }
  });
}

function doScreenshot (url, viewport) {
  const filename = `${viewport.width}x${viewport.height}.png`;
  captureWebsite.file(url, `${screenshotsDir}/${filename}`, viewport);
}
